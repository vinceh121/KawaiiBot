package me.vinceh121.lolijiiva

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.thenException
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageEmbed

abstract class AbstractTenorCommand : ICommand {
    abstract val search: String

    open fun makeText(ctx: CommandContext): String? {
        return null
    }

    open fun errMsg(ctx: CommandContext): Boolean {
        return false
    }

    final override fun run(ctx: CommandContext) {
        val e = errMsg(ctx)
        if (!e) {
            KawaiiBot.tenor
                    .getUrl(search)
                    .thenAccept {
                        ctx.sendEmbed {
                            setTitle(makeText(ctx))
                            setImage(it)
                        }
                    }
                    .thenException { ctx.send("I couldn't get a GIF ;-;\n*and not a jif*") }
        }
    }
}
