package me.vinceh121.lolijiiva

import me.alexflipnote.kawaiibot.extensions.thenException
import me.alexflipnote.kawaiibot.extensions.json
import me.alexflipnote.kawaiibot.utils.RequestUtil
import org.json.JSONObject
import okhttp3.*
import java.util.concurrent.CompletableFuture
import org.slf4j.LoggerFactory
import java.net.URLEncoder
import java.util.Random

class TenorApi(val apiKey: String, val mediaType: String) {
    val LOG = LoggerFactory.getLogger("TenorApi")
    val RANDOM = Random()

    fun getUrl(search: String): CompletableFuture<String> {
        val fut = CompletableFuture<String>()
        RequestUtil.get("https://api.tenor.com/v1/search?key=${apiKey}&q=${URLEncoder.encode(search, "utf-8")}")
            .thenAccept {
                try {
                    val res = it.json()?.getJSONArray("results")
                        ?: throw RuntimeException("Failed to get GIFs list")

                    if (res.length() == 0) {
                        throw RuntimeException("No results")
                    }

                    val nbr = RANDOM.nextInt(res.length())

                    val obj = res.getJSONObject(nbr)

                    val url = obj.getJSONArray("media").getJSONObject(0).getJSONObject(mediaType).getString("url")

                    fut.complete(url)
                } catch (ex: RuntimeException) {
                    fut.completeExceptionally(ex)
                }
            }.thenException {
                LOG.error("Failed to get tenor results ", it)
                fut.completeExceptionally(RuntimeException("Failed to make REST call"))
            }
        return fut
    }
}