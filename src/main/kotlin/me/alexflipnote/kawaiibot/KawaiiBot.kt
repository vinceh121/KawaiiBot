package me.alexflipnote.kawaiibot

import me.alexflipnote.kawaiibot.utils.CommandClasspathScanner
//import me.alexflipnote.kawaiibot.utils.SpeedyBoi
import me.aurieh.ichigo.core.CommandHandler
import net.dv8tion.jda.api.sharding.DefaultShardManagerBuilder
import net.dv8tion.jda.api.sharding.ShardManager
import net.dv8tion.jda.api.requests.GatewayIntent
import net.dv8tion.jda.api.entities.Activity
import okhttp3.OkHttpClient
import org.slf4j.LoggerFactory
import java.awt.Color
import java.io.FileInputStream
import java.io.IOException
import java.util.*
import javax.security.auth.login.LoginException
import java.net.InetSocketAddress
import java.util.concurrent.TimeUnit

import me.vinceh121.lolijiiva.NoWeebException
import me.vinceh121.lolijiiva.TenorApi

import com.codahale.metrics.MetricFilter
import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.graphite.Graphite
import com.codahale.metrics.graphite.GraphiteReporter
import com.codahale.metrics.jvm.GarbageCollectorMetricSet
import com.codahale.metrics.jvm.MemoryUsageGaugeSet

object KawaiiBot {

    const val version = "3.2.1"
    private val bootTime = System.currentTimeMillis()
                            // vinceh121
    val developerIds = setOf(340473152259751936L)
    var LOG = LoggerFactory.getLogger("KawaiiBot")

    val config = Properties()

    lateinit var shardManager: ShardManager
    lateinit var commandHandler: CommandHandler
    lateinit var httpClient: OkHttpClient
    lateinit var tenor: TenorApi
    // lateinit var wolkeApi?: Weeb4J
    //     get() {
    //         if (wolkeApi == null) {
    //             throw NoWeebException()
    //         } else {
    //             return wolkeApi
    //         }
    //     }
    lateinit var embedColor: Color

    var otherCommandUsage = 0
    var pornUsage = 0

    val uptime: Long
        get() = System.currentTimeMillis() - bootTime

    val metricRegistry: MetricRegistry = MetricRegistry()

    @Throws(LoginException::class, IOException::class)
    @JvmStatic
    fun main(args: Array<String>) {
        if (args.isNotEmpty() && args[0].equals("--sharded", ignoreCase = true)) {
            LOG.info("Running in production mode!")
            config.load(FileInputStream("/etc/kawaii/config.properties"))
        } else {
            LOG.info("Running in development mode!")
            config.load(FileInputStream("./dev.properties"))
        }

        val defaultPrefix = config.getProperty("prefix")
        embedColor = Color.decode(config.getProperty("color", "0xC29FAF"))

        tenor = TenorApi(config.getProperty("tenor_api"), config.getProperty("tenor_media"))
        // if (wolkeApiKey != null) {
        //     LOG.info("Wolke API key present, enabling Weeb4J...")
        //     wolkeApi = Weeb4J.Builder()
        //             .setToken(TokenType.WOLKE, wolkeApiKey)
        //             .setUserAgent("KawaiiBot/$version (https://kawaiibot.xyz)")
        //             .build()
        // } else {
        //     wolkeApi = null
        // }

        if (config.containsKey("metrics_host")) {
            LOG.info("Starting metrics")

            metricRegistry.registerAll("kawaii-gc", GarbageCollectorMetricSet())
		    metricRegistry.registerAll("kawaii-mem", MemoryUsageGaugeSet())

            val graphite = Graphite(InetSocketAddress(config.getProperty("metrics_host"), Integer.parseInt(config.getProperty("metrics_port"))))
            val graphiteReporter = GraphiteReporter.forRegistry(metricRegistry)
                .prefixedWith("kawaii")
                .convertRatesTo(TimeUnit.MILLISECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .filter(MetricFilter.ALL)
                .build(graphite)

            graphiteReporter.start(config.getProperty("metrics_interval").toLongOrNull() ?: throw IllegalArgumentException("Could not parse metrics interval"), TimeUnit.MINUTES)
        }

        val shardIdentifyDelay = config.getProperty("shardidentifydelay", "5000").toLong()

        httpClient = OkHttpClient.Builder().build()
        commandHandler = CommandHandler.Builder(defaultPrefix)
                .addCommandsAll(CommandClasspathScanner.scan(this::class.java.classLoader))
                .addDevelopersAll(developerIds)
                .build()

        shardManager = DefaultShardManagerBuilder()
                .enableIntents(GatewayIntent.GUILD_MESSAGES, GatewayIntent.DIRECT_MESSAGES, GatewayIntent.GUILD_MEMBERS)
                .setShardsTotal(1)
                .setToken(config.getProperty("token"))
                .setActivity(Activity.playing(defaultPrefix + "help"))
                .addEventListeners(commandHandler)
                //.setSessionController(SpeedyBoi(shardIdentifyDelay))
                .build();
    }
}
