package me.alexflipnote.kawaiibot.extensions

import net.dv8tion.jda.api.MessageBuilder
import net.dv8tion.jda.api.entities.MessageChannel
import java.io.InputStream

fun MessageChannel.sendFile(content: String, file: InputStream, filename: String) = sendFile(file, filename).content(content)!!
