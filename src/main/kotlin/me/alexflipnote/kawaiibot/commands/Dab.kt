package me.alexflipnote.kawaiibot.commands


import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.utils.Helpers
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(description = "Dab on haters", botPermissions = [Permission.MESSAGE_EMBED_LINKS])
class Dab : AbstractTenorCommand() {
    private val comments = arrayOf("Dabs on haters", "Dabbing is so 2016", "#DabIsNotDead")
    override val search = "anime dab"

    override fun makeText(ctx: CommandContext): String? {
        return Helpers.chooseRandom(comments)
    }
}
