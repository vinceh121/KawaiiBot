package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.extensions.closing
import me.alexflipnote.kawaiibot.extensions.thenException
import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.utils.RequestUtil
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import okhttp3.RequestBody

@Command(description = "Get pics of the best boi from astolfo.life", aliases = ["al", "astolfolife"], botPermissions = [Permission.MESSAGE_EMBED_LINKS], isNSFW = true)
class Astolfo : ICommand {
    override fun run(ctx: CommandContext) {
        var type = ctx.args.nextOrNull()
        var subdom = ctx.args.nextOrNull()

        if (type == null)
            type = ""
        
        if (subdom == null)
            subdom = ""
        else
            subdom = subdom + "."

        RequestUtil.get("https://${subdom}astolfo.life/${type}")
            .thenAccept {
                val body = it.body();
                if (!it.isSuccessful || body == null) {
                    KawaiiBot.LOG.error("Invalid response from astolfo.life: ${it}")
                    ctx.send("There was an error while reaching the best boi from astolfo.life ;-;");
                    body?.close()
                } else {
                    ctx.channel.sendFile(body.byteStream(), "image.${body.contentType()?.subtype()}")
                        .submit()
                        .handle { _, _ -> body.close() }
                }
        }.thenException {
            KawaiiBot.LOG.error("astolfo.life request failed", it)
            ctx.send("Failed to get the best boi from astolfo.life :(")
        }
    }
}
