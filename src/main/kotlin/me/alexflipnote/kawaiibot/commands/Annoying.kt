package me.alexflipnote.kawaiibot.commands

import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import java.text.DecimalFormat
import java.util.*
import me.alexflipnote.kawaiibot.KawaiiBot

@Command(description = "Find out if someone is annoying or not!"/*, guildOnly = true*/)
class Annoying : ICommand {

    private val dpFormatter = DecimalFormat("0.00")

    override fun run(ctx: CommandContext) {
        val random = Random()
        val memArg = ctx.args.asMember
        val target = if (memArg == null) ctx.author else memArg.user

        random.setSeed(target.idLong)
        val isAnnoying = random.nextBoolean()
        if (isAnnoying) {
            ctx.send("**${target.name}**, is very annoying")
        } else {
            ctx.send("**${target.name}**, is a very nice person")
        }
    }
}
