package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.thenException
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(description = "Wave", botPermissions = [Permission.MESSAGE_EMBED_LINKS], guildOnly = true)
class Wave : AbstractTenorCommand() {
    override val search = "anime wave"

    override fun makeText(ctx: CommandContext): String? {
        return null
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        return false
    }
}
