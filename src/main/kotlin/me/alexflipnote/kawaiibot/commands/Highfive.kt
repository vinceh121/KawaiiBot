package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.extensions.clean
import me.alexflipnote.kawaiibot.utils.Helpers
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(
        description = "High-five someone! o/\\o ",
        botPermissions =
                [
                        Permission.MESSAGE_EMBED_LINKS,
                        Permission.MESSAGE_ATTACH_FILES,
                        Permission.MESSAGE_EXT_EMOJI],
        guildOnly = true
)
class Highfive : AbstractTenorCommand() {
    override val search = "anime high five"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.effectiveName?.clean()}**, you got a high-five from **${ctx.author.name}**"
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you trying to high-five atoms...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send(
                    "*High-fives **${ctx.author.name}** back* <:highfive_L:415309531879571468><:highfive_R:415309533708288000>"
            )
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.channel
                    .sendFile(Helpers.getImageStream("images/selffive.gif"), "selffive.gif")
                    .content("*awkward...*")
                    .queue()
            return true
        }

        return false
    }
}
