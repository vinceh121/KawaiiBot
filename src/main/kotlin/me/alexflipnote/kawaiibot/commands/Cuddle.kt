package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.alexflipnote.kawaiibot.extensions.thenException
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Cuddle someone :3 ", botPermissions = [Permission.MESSAGE_EMBED_LINKS])
class Cuddle : AbstractTenorCommand() {
    override val search = "anime cuddle"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.user?.name}**, you got a cuddle from **${ctx.author.name}**";
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you trying to cuddle the void...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("*Cuddles **${ctx.author.name}** back*")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.send("Sorry to see you alone ;-;")
            return true
        }

        return false;
    }
}
