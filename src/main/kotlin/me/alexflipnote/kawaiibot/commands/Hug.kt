package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.clean
import me.alexflipnote.kawaiibot.extensions.thenException
import me.alexflipnote.kawaiibot.utils.Helpers
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(
        description = "Give someone a hug o////o",
        botPermissions = [Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES]
)
class Hug : AbstractTenorCommand() {
    override val search = "anime hug"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.effectiveName?.clean()}**, you got a hug from **${ctx.author.name}**"
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you trying to hug thin air...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("*Hugs **${ctx.author.name}** back* ❤")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.channel
            .sendFile(
                Helpers.getImageStream("images/selfhug.gif"),
                "selfhug.gif")
                .content("Sorry to see you alone...")
                .queue()
            return true
        }

        return false;
    }
}
