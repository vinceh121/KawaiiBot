package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.clean
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.alexflipnote.kawaiibot.extensions.thenException
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Poke someone :3", botPermissions = [Permission.MESSAGE_EMBED_LINKS], guildOnly = true)
class Poke : AbstractTenorCommand() {
    override val search = "anime poke"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.effectiveName?.clean()}**, you got a poke from **${ctx.author.name}**";
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you trying to poke thin air...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("Don't poke me ;-;")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.send("You can't poke yourself... baka ;-;")
            return true
        }

        return false
    }
}
