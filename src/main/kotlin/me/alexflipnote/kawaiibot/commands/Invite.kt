package me.alexflipnote.kawaiibot.commands

import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command

@Command(description = "Invite me to your server :3")
class Invite : ICommand {
    override fun run(ctx: CommandContext) {
        ctx.send("This fork is private, please contact vinceh121: https://vinceh121.me/contact")
    }
}
