package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.extensions.sendFile
import me.alexflipnote.kawaiibot.utils.Helpers
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(
        description = "Give someone a pat! o//o",
        botPermissions = [Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES]
)
class Pat : AbstractTenorCommand() {
    override val search = "anime pat"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.user?.name}**, you got a pat from **${ctx.author.name}**"
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you trying to pat air...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.channel
                    .sendFile(Helpers.getImageStream("images/kawaiipat.gif"), "kawaiipat.gif")
                    .queue()
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.channel
                    .sendFile(
                            "Don't be like that ;-;",
                            Helpers.getImageStream("images/selfpat.gif"),
                            "selfpat.gif"
                    )
                    .queue()
            return true
        }

        return false
    }
}
