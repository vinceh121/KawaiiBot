package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Wag the tail! :3", aliases = ["tails", "wagging"], botPermissions = [Permission.MESSAGE_EMBED_LINKS])
class Wag : AbstractTenorCommand() {
    override val search = "anime tail wag"
}
