package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.alexflipnote.kawaiibot.extensions.thenException

@Command(description = "Posts a girl saying lewd", botPermissions = [Permission.MESSAGE_EMBED_LINKS], aliases = ["fuck"])
class Lewd : ICommand {

    override fun run(ctx: CommandContext) {
        KawaiiBot.tenor.getUrl("anime lewd").thenAccept {
                ctx.sendEmbed {
                    setImage(it)
                    setDescription("How lewd!")
                }
        }.thenException {
            ctx.send("I couldn't get a GIF ;-;")
        }
    }

}
