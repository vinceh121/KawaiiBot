package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.entities.AbstractAPICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission

@Command(description = "Generates an Unown Game Boy text", botPermissions = [Permission.MESSAGE_ATTACH_FILES])
class UnownGB : AbstractAPICommand() {
    override val path = "/unowngb"
}