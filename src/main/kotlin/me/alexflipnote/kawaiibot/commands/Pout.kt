package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.clean
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.alexflipnote.kawaiibot.extensions.thenException
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Pout", botPermissions = [Permission.MESSAGE_EMBED_LINKS], guildOnly = true)
class Pout : AbstractTenorCommand() {
    override val search = "anime pout"
}