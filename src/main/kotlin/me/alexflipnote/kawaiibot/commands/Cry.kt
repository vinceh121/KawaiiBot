package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(description = "Posts a crying picture when you're sad ;-;", botPermissions = [Permission.MESSAGE_EMBED_LINKS])
class Cry : AbstractTenorCommand() {
    override val search = "anime cry"
}
