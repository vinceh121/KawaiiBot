package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.clean
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.alexflipnote.kawaiibot.extensions.thenException
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Kick someone", botPermissions = [Permission.MESSAGE_EMBED_LINKS], guildOnly = true)
class Kick : AbstractTenorCommand() {
    override val search = "anime kick"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.user?.name}**, you got kicked by **${ctx.author.name}**";
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you really kicking the air...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("Stahhhp kicking me! ;-;")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.send("How even would you do that")
            return true
        }

        return false;
    }
}
