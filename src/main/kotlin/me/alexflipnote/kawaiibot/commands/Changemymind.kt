package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.entities.AbstractAPICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission

@Command(description = "Change my mind", botPermissions = [Permission.MESSAGE_ATTACH_FILES])
class Changemymind : AbstractAPICommand() {
    override val path = "/changemymind"
}
