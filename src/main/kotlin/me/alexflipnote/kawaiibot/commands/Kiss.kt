package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.alexflipnote.kawaiibot.extensions.thenException
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Kiss someone :3 ", botPermissions = [Permission.MESSAGE_EMBED_LINKS], aliases = ["kusd"])
class Kiss : AbstractTenorCommand() {
    override val search = "anime kiss"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.user?.name}**, you got a kiss from **${ctx.author.name}**";
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you trying to kiss the void...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("*Kisses **${ctx.author.name}** back* ❤")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.send("Sorry to see you alone ;-;")
            return true
        }

        return false;
    }
}
