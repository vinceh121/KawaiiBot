package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.entities.AbstractAPICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission

@Command(description = "Generates an Unown \"sans serif\" text", botPermissions = [Permission.MESSAGE_ATTACH_FILES])
class Unown : AbstractAPICommand() {
    override val path = "/unown"
}