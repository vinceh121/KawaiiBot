package me.alexflipnote.kawaiibot.commands

import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import java.text.DecimalFormat
import java.util.*
import me.alexflipnote.kawaiibot.KawaiiBot

@Command(description = "Find out your pickle size!"/*, guildOnly = true*/)
class Pickle : ICommand {

    private val dpFormatter = DecimalFormat("0.00")

    override fun run(ctx: CommandContext) {
        val random = Random()
        val memArg = ctx.args.asMember
        val target = if (memArg == null) ctx.author else memArg.user

        random.setSeed(target.idLong)
        val size = dpFormatter.format(random.nextInt(50).toDouble() / 1.17)
        if (KawaiiBot.developerIds.contains(target.idLong)) {
            ctx.send("**${target.name} is one of my masters so of couse his pickle is *HUGE***\n\n"+
                "Nevermind it's just **${size}cm**")
        } else if (memArg == null) {
            ctx.send("**${target.name}**, your pickle size is **${size}cm** \uD83C\uDF80")
        } else {
            ctx.send("**${target.name}'s** pickle size is **${size}cm** \uD83C\uDF80")
        }
    }
}
