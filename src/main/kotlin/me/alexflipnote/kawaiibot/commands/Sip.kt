package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Sipping on the haters", aliases = ["drink"], botPermissions = [Permission.MESSAGE_EMBED_LINKS])
class Sip : AbstractTenorCommand() {
    override val search = "anime sip"
}
