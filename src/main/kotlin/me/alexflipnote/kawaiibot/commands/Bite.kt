package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.clean
import me.alexflipnote.kawaiibot.extensions.thenException
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(
        description = "Bite someone :3c",
        botPermissions = [Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES]
)
class Bite : AbstractTenorCommand() {
    override val search = "anime bite"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.effectiveName?.clean()}**, you were bitten by **${ctx.author.name}**"
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you trying to bite thin air...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("Don't b-bite me ;-;")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.send("W-Why would you want to bite yourself...?")
            return true
        }

        return false;
    }
}
