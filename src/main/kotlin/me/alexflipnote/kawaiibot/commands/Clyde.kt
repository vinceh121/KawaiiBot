package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.entities.AbstractAPICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission

@Command(description = "Make a Clyde message", botPermissions = [Permission.MESSAGE_ATTACH_FILES])
class Clyde : AbstractAPICommand() {
    override val path = "/clyde"
}
