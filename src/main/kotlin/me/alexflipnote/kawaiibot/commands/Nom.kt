package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.thenException
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(description = "Nom someone :3 ", botPermissions = [Permission.MESSAGE_EMBED_LINKS])
class Nom : AbstractTenorCommand() {
    override val search = "anime nom"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.user?.name}**, you got a nom from **${ctx.author.name}**"
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you trying to nom the void...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("*Noms **${ctx.author.name}** back* ❤")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.send("Sorry to see you alone ;-;")
            return true
        }

        return false
    }
}
