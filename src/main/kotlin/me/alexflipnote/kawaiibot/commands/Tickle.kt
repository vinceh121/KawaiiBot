package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.clean
import me.alexflipnote.kawaiibot.utils.Helpers
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.alexflipnote.kawaiibot.extensions.thenException
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Tickle someone! :3", botPermissions = [Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES])
class Tickle : AbstractTenorCommand() {
    override val search = "anime tickle"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.effectiveName?.clean()}**, you got tickled by **${ctx.author.name}**";
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null || m.user.idLong == ctx.author.idLong) {
            ctx.channel.sendFile(Helpers.getImageStream("images/tickle.gif"), "tickle.gif").queue()
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("*giggles* ❤")
            return true
        }

        return false;
    }
}
