package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.clean
import me.alexflipnote.kawaiibot.utils.Helpers
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.MessageBuilder
import me.alexflipnote.kawaiibot.extensions.thenException
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Lick someone o////o", botPermissions = [Permission.MESSAGE_EMBED_LINKS])
class Lick : AbstractTenorCommand() {
    override val search = "anime lick"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.effectiveName?.clean()}**, was licked by **${ctx.author.name}**";
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.channel.sendFile(Helpers.getImageStream("images/airlick.gif"), "airlick.gif").content("Are you trying to lick air ?").queue()
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("${ctx.author.name}... w-why do you lick me ;-;")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.channel.sendFile(Helpers.getImageStream("images/selflick.gif"), "selflick.gif").queue()
            return true
        }

        return false;
    }
}
