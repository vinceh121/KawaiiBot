package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.extensions.thenException
import me.alexflipnote.kawaiibot.utils.Helpers
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(
        description = "Slap someone! o//o",
        botPermissions = [Permission.MESSAGE_EMBED_LINKS, Permission.MESSAGE_ATTACH_FILES],
        guildOnly = true
)
class Slap : AbstractTenorCommand() {
    override val search = "anime slap"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.args.asMember?.user?.name}**, you got a slap from **${ctx.author.name}**"
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Are you trying to slap a ghost...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("**${ctx.author.name}** we can no longer be friends ;-;")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.channel.sendFile(Helpers.getImageStream("images/butwhy.gif"), "butwhy.gif").queue()
            return true
        }

        return false
    }
}
