package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.extensions.closing
import me.alexflipnote.kawaiibot.extensions.thenException
import me.alexflipnote.kawaiibot.KawaiiBot
import me.alexflipnote.kawaiibot.utils.RequestUtil
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import okhttp3.RequestBody

@Command(description = "Use the awesomeness of weeb.services", aliases = ["ws"], botPermissions = [Permission.MESSAGE_EMBED_LINKS], isNSFW = true)
class Weeb : ICommand {
    override fun run(ctx: CommandContext) {
        var subdom = ctx.args.nextOrNull()
        var type = ctx.args.nextOrNull()

        if (subdom == null) {
            ctx.send("Command format: weeb <sum domain> <path type>");
            return;
        }

        if (type == null)
            type = ""
        
        RequestUtil.get("https://${subdom}.weeb.services/${type}")
            .thenAccept {
                val body = it.body();
                if (!it.isSuccessful || body == null) {
                    KawaiiBot.LOG.error("Invalid response from weeb.services: ${it}")
                    ctx.send("There was an error while getting the good stuff from weeb.services");
                    body?.close()
                } else {
                    ctx.channel.sendFile(body.byteStream(), "image.${body.contentType()?.subtype()}")
                        .submit()
                        .handle { _, _ -> body.close() }
                }
        }.thenException {
            KawaiiBot.LOG.error("weeb.services request failed", it)
            ctx.send("Failed to get the good stuff from weeb.services :(")
        }
    }
}
