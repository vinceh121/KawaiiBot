package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.utils.Helpers
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.annotations.Command
import me.vinceh121.lolijiiva.AbstractTenorCommand
import net.dv8tion.jda.api.Permission

@Command(description = "Call someone a baka", botPermissions = [Permission.MESSAGE_EMBED_LINKS])
class Baka : AbstractTenorCommand() {
    override val search = "anime bonk"

    override fun makeText(ctx: CommandContext): String? {
        return "**${ctx.author.name}**, called **${ctx.args.asMember?.user?.name}** a baka"
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        val m = ctx.args.asMember

        if (m == null) {
            ctx.send("Who are you calling a baka...?")
            return true
        } else if (m.user.idLong == ctx.jda.selfUser.idLong) {
            ctx.send("**${ctx.author.name}** how could you :'(")
            return true
        } else if (m.user.idLong == ctx.author.idLong) {
            ctx.channel
                    .sendFile(Helpers.getImageStream("images/selfbaka.jpg"), "selfbaka.jpg")
                    .queue()
            return true
        }

        return false
    }
}
