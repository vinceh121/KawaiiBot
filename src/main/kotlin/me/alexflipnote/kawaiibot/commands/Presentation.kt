package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.entities.AbstractAPICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission

@Command(description = "Lisa Simpsons' presentation", botPermissions = [Permission.MESSAGE_ATTACH_FILES])
class Presentation : AbstractAPICommand() {
    override val path = "/presentation"
}
