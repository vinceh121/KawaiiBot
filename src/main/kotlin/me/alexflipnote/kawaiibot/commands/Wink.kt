package me.alexflipnote.kawaiibot.commands

import me.alexflipnote.kawaiibot.KawaiiBot
import me.aurieh.ichigo.core.CommandContext
import me.aurieh.ichigo.core.ICommand
import me.aurieh.ichigo.core.annotations.Command
import net.dv8tion.jda.api.Permission
import me.alexflipnote.kawaiibot.extensions.thenException
import me.vinceh121.lolijiiva.AbstractTenorCommand

@Command(description = "Wink wink", botPermissions = [Permission.MESSAGE_EMBED_LINKS])
class Wink : AbstractTenorCommand() {
    override val search = "anime wink"

    override fun makeText(ctx: CommandContext): String? {
        return null;
    }

    override fun errMsg(ctx: CommandContext): Boolean {
        return false;
    }
}
